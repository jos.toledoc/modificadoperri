from django.db import models

# Create your models here.

class Perrito(models.Model):
    foto = models.ImageField(upload_to="fotop/")
    nombre = models.CharField(max_length=100)
    raza = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=500)
    estado = models.CharField(max_length=100)
