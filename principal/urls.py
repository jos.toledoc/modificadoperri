from django.urls import path
from . import views

urlpatterns = [
    path('',views.index,name="index"),
    path('login/',views.login,name="login.html"),
    path('formulario/', views.formulario,name="formulario"),
    path('formulario/registro_perrito',views.registro_perrito,name="registro_perrito"),
    path('editar_perrito/<int:id>',views.editar_perrito,name="editar_perrito"),
    path('editar_perrito/perrito_editado/<int:id>',views.perrito_editado,name="perrito_editado"),
    path('eliminar_perrito/<int:id>',views.eliminar_perrito,name="eliminar_perrito")
   
]
